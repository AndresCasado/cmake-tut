cmake_minimum_required(VERSION 3.16)
project(tutorial-5 CXX)

set(CMAKE_CXX_STANDARD 14)

list(APPEND CMAKE_PREFIX_PATH "/home/jjcasmar/projects/cmake-tut/install")

find_package(smalllib REQUIRED)

add_executable(smallapp main.cpp)
target_link_libraries(smallapp smalllib::smalllib-4)

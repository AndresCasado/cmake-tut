#include "lib.h"

#include <spdlog/spdlog.h>

Eigen::Vector2d SmallLib::norm()
{
    Eigen::Vector2d v{1, 1};
    spdlog::info("Norm is {}", v.norm());
    return v;
}

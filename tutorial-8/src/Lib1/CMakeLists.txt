set(SOURCES
    )

set(HEADERS
    )

set(PUBLIC_HEADERS
    )


add_library(Lib1 ${SOURCES} ${HEADERS} ${PUBLIC_HEADERS})
target_link_libraries(Lib1 Eigen3::Eigen)

# The include directory is one above this file. That way I can do
# #include <Lib/header.h>
# and that should would in build and install mode
# Installation of public headers will go into ${CMAKE_INSTALL_PREFIX}/include/Lib
target_include_directories(Lib1 PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/..>
  $<INSTALL_INTERFACE:include>
)
